using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorConsole.Operations;

namespace CalculatorConsoleTests
{
    [TestClass]
    public class AdditionTest
    {
        [TestMethod]
        public void Add_20and30_50return()
        {
            Addition addition = new Addition();

            double act = addition.Calculate(20, 30);

            Assert.AreEqual(50, act);
        }
    }

    [TestClass]
    public class SubtractionTest
    {
        [TestMethod]
        public void Sub_30and20_10return()
        {
            Subtraction subtraction = new Subtraction();

            double act = subtraction.Calculate(20, 30);

            Assert.AreEqual(10, act);
        }
    }

    [TestClass]
    public class MultiplicationTest
    {
        [TestMethod]
        public void Mul_3and2_6return()
        {
            Multiplication multiplication = new Multiplication();

            double act = multiplication.Calculate(3, 2);

            Assert.AreEqual(6, act);
        }
    }

    [TestClass]
    public class DivisionTest
    {
        [TestMethod]
        public void Div_8and2_4return()
        {
            Division division = new Division();

            double act = division.Calculate(2, 8);

            Assert.AreEqual(4, act);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Div_8and0_ExeptionReturn()
        {
            Division division = new Division();

            double act = division.Calculate(0, 8);
        }
    }
}
