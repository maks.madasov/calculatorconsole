﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorConsole.Operations;
using CalculatorConsole;
using System.Collections.Generic;

namespace CalculatorConsoleTests
{
    [TestClass]
    public class StringParserTests
    {
        [TestMethod]
        public void CorrectInputString()
        {
            StringParser stringParser = new StringParser();
            string str = "2+2-5*1/10";
            List<OperationBase> list = new List<OperationBase>() {
                new Addition(),
                new Subtraction(),
                new Multiplication(),
                new Division()
            };
            string[] expected = new string[] { "2", "+", "2", "-", "5", "*", "1", "/", "10", ";" };

            string[] element = stringParser.Parse(str, list);

            CollectionAssert.AreEqual(expected, element);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void IncorrectInputString()
        {
            StringParser stringParser = new StringParser();
            string str = "2+2-5*х";
            List<OperationBase> list = new List<OperationBase>() {
                new Addition(),
                new Subtraction(),
                new Multiplication(),
                new Division()
            };

            string[] element = stringParser.Parse(str, list);
        }
    }
}