﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorConsole;
using CalculatorConsole.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorConsoleTests
{
    [TestClass]
    public class ExpressionSolverTests
    {
        [TestMethod]
        public void SolveTest()
        {
            List<OperationBase> list = new List<OperationBase>()
            {
                new Addition(),
                new Subtraction(),
                new Multiplication(),
                new Division()
            };
            OperationsManager operationsManager = new OperationsManager(list);
            ExpressionSolver expressionSolver = new ExpressionSolver(operationsManager);
            string[] arrayStr = new string[] { "(", "5", "+", "5", ")", "*", "2", "-", "(", "10", "-", "2", ")", ";" };

            double act = expressionSolver.Solve(arrayStr);

            Assert.AreEqual(12, act);
        }
    }
}