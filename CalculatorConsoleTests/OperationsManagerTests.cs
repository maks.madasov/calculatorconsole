﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorConsole;
using CalculatorConsole.Operations;
using System.Collections.Generic;

namespace CalculatorConsoleTests
{
    [TestClass]
    public class OperationsManagerTests
    {
        [TestMethod]
        public void GetRang_Add_1return()
        {
            List<OperationBase> list = new List<OperationBase>()
            {
                new Addition(),
                new Subtraction(),
                new Multiplication(),
                new Division()
            };
            OperationsManager operationsManager = new OperationsManager(list);

            int act = operationsManager.GetOperationRang("+");

            Assert.AreEqual(1, act);
        }

        [TestMethod]
        public void GetRang_Hooks_0return()
        {
            List<OperationBase> list = new List<OperationBase>()
            {
                new Addition(),
                new Subtraction(),
                new Multiplication(),
                new Division()
            };
            OperationsManager operationsManager = new OperationsManager(list);

            int act = operationsManager.GetOperationRang("(");

            Assert.AreEqual(0, act);
        }

        [TestMethod]
        public void CalculateTest_sub_5and1_4return()
        {
            List<OperationBase> list = new List<OperationBase>()
            {
                new Addition(),
                new Subtraction(),
                new Multiplication(),
                new Division()
            };
            OperationsManager operationsManager = new OperationsManager(list);

            double act = operationsManager.Calculate(1, 5, "-");

            Assert.AreEqual(4, act);
        }
    }
}