﻿using System;
using System.Collections.Generic;
using CalculatorConsole.Operations;

namespace CalculatorConsole
{
    class Calculator
    {
        private OperationsManager operationsManager;
        private ExpressionSolver expressionSolver;
        private StringParser stringParser;

        public Calculator()
        {
            operationsManager = new OperationsManager(new List<OperationBase> {
                new Addition(),
                new Subtraction(),
                new Multiplication(),
                new Division()
            });
            expressionSolver = new ExpressionSolver(operationsManager);
            stringParser = new StringParser();
        }

        public double Run(string str) 
        {
            double result;
            try
            {
                string[] elements = stringParser.Parse(str, operationsManager.GetAllOperations());
                result = expressionSolver.Solve(elements);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error: {e.Message}");
                return -1;
            }
            return result;
        }
    }
}
