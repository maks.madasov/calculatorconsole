﻿using System;
using System.Collections.Generic;
using CalculatorConsole.Operations;
using System.Text.RegularExpressions;

namespace CalculatorConsole
{
    public class StringParser
    {
        public string[] Parse(string str, List<OperationBase> operationBases)
        {
            str = str.Trim();
            int i = 0;
            bool coin = false;
            bool brack = false;
            foreach (var c in str.ToCharArray())
            {
                coin = false;
                if (c == '-' && i == 0)
                {
                    i++;
                    continue;
                }
                else if (brack == true && c == '-')
                {
                    i++;
                    brack = false;
                    continue;
                }

                foreach (var o in operationBases)
                {
                    if (o.Op[0] == c || c == '(' || c == ')')
                    {
                        str = str.Insert(i, " ");
                        i++;
                        str = str.Insert(i + 1, " ");
                        i++;
                        coin = true;
                        if (c == '(')
                            brack = true;
                        else
                            brack = false;
                        break;
                    }
                }
                if (!(char.IsDigit(c) || c == ' ' || c == ',' || coin == true))
                {
                    throw new Exception("Incorrect input");
                }
                i++;
            }
            str = Regex.Replace(str, @"\s+", " ");
            str = str.Insert(str.Length, " ;");
            string[] tmp = str.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            return tmp;
        }
    }
}
