﻿using System.Collections.Generic;
using System;

namespace CalculatorConsole
{
    public class ExpressionSolver
    {
        private Stack<double> numbers = new Stack<double>();
        private Stack<string> operation = new Stack<string>();
        private OperationsManager operationsManager;
        private int counter = 1;

        public ExpressionSolver(OperationsManager oM)
        {
            operationsManager = oM;
        }

        public double Solve(string[] elements)
        {
            foreach (var element in elements)
            {
                OperationCheck(element);
                ExecutionAllOperations();
            }
            while (operation.Count > 0)
            {
                if (elements.Length == 2)
                {
                    break;
                }
                ExecuteOperationWithoutPop();
            }
            return numbers.Pop();
        }
        private void ExecutionAllOperations()
        {
            while (operation.Count > counter)
            {
                if (operation.Peek() == ";")
                {
                    operation.Pop();
                    break;
                }
                else if (GetPreviosOperation() == "(" || operation.Peek() == "(")
                {
                    counter++;
                    break;
                }
                else if (operation.Peek() == ")")
                {
                    operation.Pop();
                    if (counter > 1)
                        counter--;
                    while (operation.Peek() != "(") 
                    {
                        ExecuteOperationWithoutPop();
                    }
                    operation.Pop();
                    if (counter > 1)
                        counter--;
                    break;
                }
                else if (GetCurrentRang() <= GetPreviosRang())
                {
                    ExecuteOperationWithPop();
                }
                else
                {
                    counter++;
                    break;
                }
            }
        }
        private int GetCurrentRang()
        {
            return operationsManager.GetOperationRang(operation.Peek());
        }
        private int GetPreviosRang()
        {
            string tmp = operation.Pop();
            int r = GetCurrentRang();
            operation.Push(tmp);
            return r;
        }
        private string GetPreviosOperation() 
        {
            string tmp = operation.Pop();
            string tmp2 = operation.Peek();
            operation.Push(tmp);
            return tmp2;
        }
        private void ExecuteOperationWithPop()
        {
            string tmp = operation.Pop();
            ExecuteOperationWithoutPop();
            operation.Push(tmp);
        }
        private void ExecuteOperationWithoutPop()
        {
            numbers.Push(operationsManager.Calculate(numbers.Pop(), numbers.Pop(), operation.Pop()));
            if (counter > 1)
                counter--;
        }
        private void OperationCheck(string element)
        {
            if (double.TryParse(element, out double number))
            {
                numbers.Push(number);
            }
            else
            {
                operation.Push(element);
            }
        }
    }
}
