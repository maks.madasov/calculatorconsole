﻿namespace CalculatorConsole.Operations
{
    public class Addition : OperationBase
    {
        public Addition() : base("+", 1) { }
        public override double Calculate(double first, double second)
        {
            return first + second;
        }
    }
}
