﻿namespace CalculatorConsole.Operations
{
    abstract public class OperationBase
    {
        public readonly string Op;
        public readonly int Rang;
        public OperationBase(string op, int r)
        {
            this.Op = op;
            this.Rang = r;
        }
        abstract public double Calculate(double first, double second);
    }
}
