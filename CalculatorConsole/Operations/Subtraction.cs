﻿namespace CalculatorConsole.Operations
{
    public class Subtraction : OperationBase
    {
        public Subtraction() : base("-", 1) { }
        public override double Calculate(double first, double second)
        {
            return second - first;
        }
    }
}
