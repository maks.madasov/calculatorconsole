﻿using System;

namespace CalculatorConsole.Operations
{
    public class Division : OperationBase
    {
        public Division() : base("/", 2) { }
        public override double Calculate(double first, double second)
        {
            if (first == 0)
                throw new Exception("Dividing by zero");
            return second / first;
        }
    }
}
