﻿namespace CalculatorConsole.Operations
{
    public class Multiplication : OperationBase
    {
        public Multiplication() : base("*", 2) { }
        public override double Calculate(double first, double second)
        {
            return first * second;
        }
    }
}
