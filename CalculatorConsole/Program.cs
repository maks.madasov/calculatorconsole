﻿using System;
using System.Linq;

namespace CalculatorConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Calculator c = new Calculator();
            Console.WriteLine($"Otvet: {c.Run(Console.ReadLine())}");
            //Console.WriteLine($"Otvet: {c.Run("1+2*(3+4/2-(1+2))*2+1")}");
        }
    }
}
