﻿using System.Collections.Generic;
using CalculatorConsole.Operations;

namespace CalculatorConsole
{
    public class OperationsManager
    {
        List<OperationBase> Operations;
        public OperationsManager(List<OperationBase> operations)
        {
            this.Operations = operations;
        }
        public List<OperationBase> GetAllOperations()
        {
            return Operations;
        }
        public int GetOperationRang(string str)
        {
            if (str == "(" || str == ")")
            {
                return 0;
            }
            return Operations.Find(x => x.Op == str).Rang;
        } 
        public double Calculate(double first, double second, string op)
        {
            return Operations.Find(x => x.Op == op).Calculate(first, second);
        }
    }
}
